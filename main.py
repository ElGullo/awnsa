import os
import feedparser
from time import sleep
import random
import re
from datetime import datetime
from telegram.ext import Updater, CommandHandler
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

class NewsParser():
    def __init__(self):
        self.ansa_url = "https://www.ansa.it/sito/ansait_rss.xml"
        self.base_path = os.path.dirname(os.path.abspath(__file__))
        self.assets_path = os.path.join(self.base_path, "assets")
        self.assets_type = ["emojis", "idioms", "postfixes"]
        self.assets = {t: self.load_asset(self.assets_path, t) for t in self.assets_type}
        self.words_ita = set(self.load_asset(self.assets_path, "words_ita"))
        self.update_feed()
    
    def check_update(self):
        delta = datetime.now() - self.last_update
        if delta.seconds > 300:
            self.update_feed()

    def update_feed(self):
        self.feed = feedparser.parse(self.ansa_url)
        self.last_update = datetime.now()

    def load_asset(self, base_path, name):
        file_path = os.path.join(base_path, name)
        if os.path.isfile(file_path):
            with open(file_path) as f:
                return list(set(f.read().splitlines()))
        else:
            return []

    def prob(self, x):
        return random.random() < x

    def tokenize(self, sentence):
        return sentence.split()

    def intro(self):
        if self.prob(0.7):
            return random.choice(self.assets["emojis"])
        else:
            return random.choice(self.assets["idioms"])

    def outro(self):
        if self.prob(0.3):
            return random.choice(self.assets["emojis"])
        else:
            return random.choice(self.assets["idioms"])

    def replace_r(self, token):
        new_token = ''
        for letter in token:
            if letter == 'r':
                if self.prob(0.6):
                    new_token += 'w'
                else:
                    new_token += 'r'
            else:
                new_token += letter
        return new_token

    def mask_rr(self, token):
        return token.replace('rr', '__RRMASK__')

    def unmask_rr(self, token):
        return token.replace('__RRMASK__', 'rr')

    def add_postfix(self, token, postfix):
        match = re.match(r'^(.+?)(\W+)$', token)
        if match:
            return match.groups()[0] + postfix + match.groups()[1]
        else:
            return token + postfix

    def process_token(self, token):
        #add postfixes
        if token[0].isupper():
            if self.prob(0.8) and len(token)>3 and token.lower() not in self.words_ita:
                return self.add_postfix(token, random.choice(self.assets["postfixes"]))
            else:
                return token

        if 'rr' in token:
            if self.prob(0.6):
                token = token.replace('rr', 'ww')
            else:
                token = self.mask_rr(token)

        if 'r' in token:
            token = self.replace_r(token)

        token = self.unmask_rr(token)
        return token

    def process_sentence(self, sentence):
        tokens = self.tokenize(sentence)
        return ' '.join([self.process_token(t) for t in tokens])


    def process_news(self, news):
        return f"""{self.intro()}
{self.process_sentence(news.title)}
{self.process_sentence(news.summary)}
{self.outro()}
"""

    def get_random_news(self):
        self.check_update()
        random_news = random.choice(self.feed.entries)
        return self.process_news(random_news)

    def get_latest_news(self):
        self.check_update()
        latest_news = self.feed.entries[0]
        return self.process_news(latest_news)


class AwnsaNuwusBot():
    def __init__(self):
        self.parser = NewsParser()

        self.updater = Updater(token='1700887963:AAG1eyR6nLDHmP-cteRAfr0rUIGRFwotLus', use_context=True)
        self.dispatcher = self.updater.dispatcher

        self.start_handler = CommandHandler('start', self.start_message)
        self.latest_news_handler = CommandHandler(['latest', 'latest_news', 'ln'], self.latest_news)
        self.random_news_handler = CommandHandler(['news', 'random', 'random_news', 'rn'], self.random_news)

        self.dispatcher.add_handler(self.start_handler)
        self.dispatcher.add_handler(self.latest_news_handler)
        self.dispatcher.add_handler(self.random_news_handler)

    def start_message(self, update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="♥ Hiiii!! UwU ♥")

    def latest_news(self, update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text=self.parser.get_latest_news())

    def random_news(self, update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text=self.parser.get_random_news())
    
    def start_polling(self):
        self.updater.start_polling()
    
if __name__ == "__main__":
    print("Polling...")
    bot = AwnsaNuwusBot()
    bot.start_polling()